#!/bin/bash

(/export/home1/NoCsBack/dtai/jodv/fo-symmetries/binaries/install/bin/idp -e "stdoptions.symmetrybreaking=\"static\"; main()" $1 $2 2>&1 ) | awk '
/Constructing symmetry breakers/ {print $5}
/Breaking/ {print $2}
/Also breaking/ {print $3}
/Ending construction of symmetry breakers/ {print $7}
/Number of models:/ {print $4}
/bestvalue/ {print $1}
/nrmodels/ {print $1}
/saucy nodes:/ {print $3}
/saucy edges:/ {print $3}
' | sed 's/ms//g' | tr '\012' ','
