#!/bin/bash

(cat $1 | /export/home1/NoCsBack/dtai/jodv/fo-symmetries/binaries/breakid/src/BreakID - -asp -v 2 3>&1 1>&2- 2>&3- ) 2> $1.bid | awk '
/**** symmetry generators detected:/ {print $5}
/**** subgroups detected:/ {print $4} 
/**** matrices detected:/ {print $4} 
/**** row swaps detected:/ {print $5} 
/**** extra binary symmetry breaking clauses added:/ {print $8}
/**** regular symmetry breaking clauses added:/ {print $7}
/**** row interchangeability breaking clauses added:/ {print $7}
/**** total symmetry breaking clauses added:/ {print $7}
/**** auxiliary variables introduced:/ {print $5}
/**** Number of nodes:/ {print $5}
/**** Number of edges:/ {print $5}
' | tr '\012' ','
