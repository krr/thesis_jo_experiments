#include <fstream>
#include <sstream>
#include <iterator>
#include <cstring>
#include <stdlib.h>

#include "Graph.hpp"
#include "global.hpp"
#include "Algebraic.hpp"
#include "Theory.hpp"
#include "Breaking.hpp"

using namespace std;

void setFixedLits(std::string& filename) {
  if (verbosity > 0) {
    std::clog << "*** Reading fixed variables: " << filename << std::endl;
  }

  ifstream file(filename);
  if (!file) {
    gracefulError("No fixed variables file found.");
  }
  
  fixedLits.clear();
  string line;
  while (getline(file, line)) {
    istringstream iss(line);
    int l;
    while (iss >> l) {
      fixedLits.push_back(encode(l));
      fixedLits.push_back(encode(-l));
    }
  }
}

namespace options {
  // option strings:
  string nointch = "-no-row";
  string nobinary = "-no-bin";
  string formlength = "-s";
  string verbosity = "-v";
  string timelim = "-t";
  string help = "-h";
  string nosmall = "-no-small";
  string fixedvars = "-fixed";
  string onlybreakers = "-print-only-breakers";
  string aspinput = "-asp";
}

void printUsage() {
  std::clog << "BreakID version " << VERSION << std::endl;
  std::clog << "Usage: ./BreakID <cnf-file> " <<
          "[" << options::help << "] " <<
          "[" << options::nointch << "] " <<
          "[" << options::nobinary << "] " <<
          "[" << options::nosmall << "] " <<
          "[" << options::formlength << " <nat>] " <<
          "[" << options::timelim << " <nat>] " <<
          "[" << options::verbosity << " <nat>] " <<
          "[" << options::fixedvars << " <file with fixed vars>] " <<
          "[" << options::onlybreakers << "] " <<
          "[" << options::aspinput << "] " <<
          "\n";
  std::clog << "\nOptions:\n";
  std::clog << options::help << "\n  ";
  std::clog << "Display this help message instead of running BreakID.\n";
  std::clog << options::nointch << "\n  ";
  std::clog << "Disable detection and breaking of row interchangeability.\n";
  std::clog << options::nobinary << "\n  ";
  std::clog << "Disable construction of additional binary symmetry breaking clauses based on stabilizer subgroups.\n";
  std::clog << options::nosmall << "\n  ";
  std::clog << "Disable small symmetry breaking clauses, use Shatter's translation instead.\n";
  std::clog << options::formlength << " <default: " << symBreakingFormLength << ">\n  ";
  std::clog << "Limit the size of the constructed symmetry breaking formula's, measured as the number of auxiliary variables introduced. <-1> means no symmetry breaking.\n";
  std::clog << options::timelim << " <default: " << timeLim << ">\n  ";
  std::clog << "Upper limit on time spent by Saucy detecting symmetry measured in seconds.\n";
  std::clog << options::verbosity << " <default: " << verbosity << ">\n  ";
  std::clog << "Verbosity of the output. <0> means no output other than the CNF augmented with symmetry breaking clauses.\n";
  std::clog << options::fixedvars << " <default: none>\n  ";
  std::clog << "File with a list of variables that should be fixed, separated by whitespace.\n";
  std::clog << options::onlybreakers << "\n  ";
  std::clog << "Do not print original theory, only the symmetry breaking clauses.\n";
  std::clog << options::aspinput << "\n  ";
  std::clog << "Parse input in the smodels-lparse intermediate format instead of DIMACS.\n";
  gracefulError("");
}

void parseOptions(int argc, char *argv[]) {
  if (argc < 2) {
    printUsage();
  }

  for (int i = 1; i < argc; ++i) {
    string input = argv[i];
    if (0 == input.compare(options::nobinary)) {
      useBinaryClauses = false;
    } else if (0 == input.compare(options::nointch)) {
      useMatrixDetection = false;
    } else if (0 == input.compare(options::onlybreakers)) {
      onlyPrintBreakers = true;
    } else if (0 == input.compare(options::nosmall)) {
      useShatterTranslation = true;
    } else if (0 == input.compare(options::formlength)) {
      symBreakingFormLength = stoi(argv[i + 1]);
    } else if (0 == input.compare(options::timelim)) {
      timeLim = stoi(argv[i + 1]);
    } else if (0 == input.compare(options::verbosity)) {
      verbosity = stoi(argv[i + 1]);
    } else if (0 == input.compare(options::help)) {
      printUsage();
    } else if (0 == input.compare(options::fixedvars)) {
      string filename = argv[i + 1];
      setFixedLits(filename);
    } else if (0 == input.compare(options::aspinput)) {
      aspinput = true;
    }
  }

  if (verbosity > 1) {
    std::clog << "Options used: " <<
            (useMatrixDetection ? "" : options::nointch) << " " <<
            (useBinaryClauses ? "" : options::nobinary) << " " <<
            (onlyPrintBreakers ? options::onlybreakers : "") << " " <<
            (useShatterTranslation ? options::nosmall : "") << " " <<
            options::formlength << " " << symBreakingFormLength << " " <<
            options::timelim << " " << timeLim << " " <<
            options::verbosity << " " << verbosity << " " <<
            (fixedLits.size()==0 ? "" : options::fixedvars) << " " <<
            options::aspinput << " " << aspinput << " " <<
            std::endl;
  }
}

// ==== main

bool optionsCompatible(){
  if(aspinput && onlyPrintBreakers){
    std::stringstream ss;
    ss << "Options " << options::aspinput << " and " << options::onlybreakers << " are incompatible since asp output is intertwined with extra clauses.\n";
    gracefulError(ss.str());
    return false;
  }
  return true;
}

void mktmpfile(string& filename_) {
  /* Get TMPDIR env variable or fall back to /tmp/ */
  std::stringstream ss;
  auto tmpdir = getenv("TMPDIR");
  if (tmpdir == NULL){
    ss << "/tmp";
  } else{
    ss << tmpdir;
  }
  ss << "/" << "breakid.tempfile.";
  srand ( time(NULL) );
  //REALLY RANDOM TMPFILE
  ss << rand()<< rand()<< rand();
  ss << "XXXXXXX";

  auto tmp = std::string(ss.str());
  char * filename = new char[tmp.size() + 1];
  std::copy(tmp.begin(), tmp.end(), filename);
  filename[tmp.size()] = '\0'; // don't forget the terminating 0

  filename_ = mktemp(filename);

  // don't forget to free the string after finished using it
  delete[] filename;

  if (verbosity > 0) {
    std::clog << "*** Reading stdin. Using temporary file: " << filename_ << '\n';
  }
  std::ofstream out(filename_);

  std::string line;
  while (std::getline(std::cin, line)) {
    out << line << std::endl;
  }
  out.close();
}

int main(int argc, char *argv[]) {
  parseOptions(argc, argv);
  if (not optionsCompatible()) {
    return 1;
  }

  time(&startTime);
  string filename_ = argv[1];

  bool tmpfile = (filename_.size() == 1 && filename_.front() == '-');

  if (tmpfile) {
    mktmpfile(filename_);
  }

  sptr<Specification> theory;
  if(aspinput){
    theory = make_shared<LogicProgram>(filename_);
  } else{
    theory = make_shared<CNF>(filename_);
  }

  if (verbosity > 3) {
    theory->getGraph()->print();
  }

  if (verbosity > 0) {
    std::clog << "**** symmetry generators detected: " << theory->getGroup()->getSize() << std::endl;
    if (verbosity > 2) {
      theory->getGroup()->print();
    }
    std::clog << "*** Detecting subgroups..." << std::endl;
  }
  vector<sptr<Group> > subgroups;
  theory->getGroup()->getDisjointGenerators(subgroups);
  if (verbosity > 0) {
    std::clog << "**** subgroups detected: " << subgroups.size() << std::endl;
  }

  if (verbosity > 1) {
    for (auto grp : subgroups) {
      std::clog << "group size: " << grp->getSize() << " support: " << grp->getSupportSize() << std::endl;
      if (verbosity > 2) {
        grp->print();
      }
    }
  }
  
  theory->cleanUp(); // improve some memory overhead
  
  uint totalNbMatrices = 0;
  uint totalNbRowSwaps = 0;
  
  Breaker brkr(theory);
  for (auto grp : subgroups) {
    if (grp->getSize() > 1 && useMatrixDetection) {
      if (verbosity > 1) {
        std::clog << "*** Detecting row interchangeability..." << std::endl;
      }
      theory->setSubTheory(grp);
      grp->addMatrices();
      totalNbMatrices += grp->getNbMatrices();
      totalNbRowSwaps += grp->getNbRowSwaps();
    }
    if (verbosity > 1) {
      std::clog << "*** Constructing symmetry breaking formula..." << std::endl;
    }
    grp->addBreakingClausesTo(brkr);
    grp.reset();
  }
  
  if (verbosity > 0) {
    std::clog << "**** matrices detected: " << totalNbMatrices << std::endl;
    std::clog << "**** row swaps detected: " << totalNbRowSwaps << std::endl;
    std::clog << "**** extra binary symmetry breaking clauses added: " << brkr.getNbBinClauses() << "\n";
    std::clog << "**** regular symmetry breaking clauses added: " << brkr.getNbRegClauses() << "\n";
    std::clog << "**** row interchangeability breaking clauses added: " << brkr.getNbRowClauses() << "\n";
    std::clog << "**** total symmetry breaking clauses added: " << brkr.getAddedNbClauses() << "\n";
    std::clog << "**** auxiliary variables introduced: " << brkr.getAuxiliaryNbVars() << "\n";
    std::clog << "*** Printing resulting theory with symmetry breaking clauses..." << std::endl;
  }

  brkr.print(filename_);

  if(tmpfile){
    if(verbosity>0){
      std::clog << "*** removing temporary file\n";
      std::remove(filename_.c_str());
    }
  }

  return 0;
}
