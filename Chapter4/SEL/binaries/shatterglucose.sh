#!/bin/bash

cnf=$1

ddpsize=$(wc -c <"$cnf.ddp")
brksize=$(wc -c <"$cnf.brk")


if [ $ddpsize == $brksize ]; then
	echo "nosym"
else
/export/home1/NoCsBack/dtai/jodv/breakid/glucose_static $cnf.brk 2>&1 | awk '
/generators:/ {print $7}
/c restarts/ {print $4} 
/c decisions/ {print $4} 
/c conflicts/ {print $4}
/c symgenconfls/ {print $4}
/c symselconfls/ {print $4}
/c propagations/ {print $4} 
/c symgenprops/ {print $4}
/c symselprops/ {print $4}
/SATISFIABLE/ {print $2}
/ERROR/ {print $1}
' | tr '\012' ','
fi
