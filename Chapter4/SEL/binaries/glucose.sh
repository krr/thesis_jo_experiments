#!/bin/bash

cnf=$1
line=$(head -n 1 $cnf)

#if [ "$line" == "c number of breaking clauses added: 0" ]; then
#	echo "nosym"
#else
/export/home1/NoCsBack/dtai/jodv/breakid/glucose_static $cnf 2>&1 | awk '
/generators:/ {print $7}
/c restarts/ {print $4} 
/c decisions/ {print $4} 
/c conflicts/ {print $4}
/c symgenconfls/ {print $4}
/c symselconfls/ {print $4}
/c propagations/ {print $4} 
/c symgenprops/ {print $4}
/c symselprops/ {print $4}
/SATISFIABLE/ {print $2}
/ERROR/ {print $1}
' | tr '\012' ','
#fi
