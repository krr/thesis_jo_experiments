#!/bin/bash

cnf=$1

(/export/home1/NoCsBack/dtai/jodv/breakid/shatter -s $cnf.ddp 3>&1 1>&2- 2>&3- ) 2> $cnf.brk | awk '
/generators/ {print $3}
/vertices/ {print $3}
/edges/ {print $3}
' | tr '\012' ','
