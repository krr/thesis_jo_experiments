/*******************************************************************************************[Map.h]
Copyright (c) 2006-2010, Niklas Sorensson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************************************/

#pragma once

#include "mtl/IntTypes.h"
#include "mtl/Vec.h"

namespace Glucose {

template <typename T>
struct matrix {
    int nr; // number of rows
    int nc; // number of columns
    vec<T> m; // such that matrix[r[l]*columns+c[l]] == l

    matrix():nr(0),nc(0){
        m.growTo(0);
    }
    
    void growTo(int nbRows, int nbColumns){
        if(nbRows*nbColumns<=m.size()){
            return;
        }
        nr=nbRows;
        nc=nbColumns;
        m.growTo(nr*nc);
    }

    void growTo(int nbRows, int nbColumns, const T& e){
        if(nbRows*nbColumns<=m.size()){
            return;
        }
        nr=nbRows;
        nc=nbColumns;
        m.growTo(nr*nc,e);
    }

    void assign(int nbRows, int nbColumns, const T& e){
        nr=nbRows;
        nc=nbColumns;
        m.assign(nr*nc,e);
    }

    void set(int r, int c, const T& e){
        assert(r<nr);
        assert(r>=0);
        assert(c<nc);
        assert(c>=0);
        m[r*nc+c]=e;
    }

    T& get(int r, int c){
        assert(r<nr);
        assert(r>=0);
        assert(c<nc);
        assert(c>=0);
        return m[r*nc+c];
    }

    void copyTo(matrix<T>& other){
        other.nr=nr;
        other.nc=nc;
        other.m=m;
    }

    inline int size(){
        return m.size();
    }
};

//=================================================================================================
}
