// Copyright (C) 2011  Davis E. King (davis@dlib.net)
// License: Boost Software License   See LICENSE.txt for the full license.

#pragma once

#include "mtl/Matrix.h"
#include <deque>
#include <limits>

namespace Glucose {

// ----------------------------------------------------------------------------------------

    inline void compute_slack(
        int x,
        vec<int>& slack,
        vec<int>& slackx,
        matrix<int>& cost,
        vec<int>& lx,
        vec<int>& ly
    ){
        for (int y = 0; y < cost.nc; ++y){
            if (lx[x] + ly[y] - cost.get(x,y) < slack[y]){
                slack[y] = lx[x] + ly[y] - cost.get(x,y);
                slackx[y] = x;
            }
        }
    }

// ----------------------------------------------------------------------------------------

    int max_cost_assignment (matrix<int>& cost, vec<int>& xy){
        if (cost.size() == 0){
            xy.clear();
            return 0;
        }

        // This algorithm only works if the elements of the cost matrix can be reliably 
        // compared using operator==. However, comparing for equality with floating point
        // numbers is not a stable operation. Luckily, we use an integer cost matrix :)
        assert(cost.nr==cost.nc);

        vec<int> lx, ly;
        vec<int> yx;
        vec<char> S, T;
        vec<int> slack;
        vec<int> slackx;
        vec<int> aug_path;

        // Initially, nothing is matched. 
        xy.assign(cost.nc, -1);
        yx.assign(cost.nc, -1);
        /*
            We maintain the following invariant:
                Vertex x is matched to vertex xy[x] and
                vertex y is matched to vertex yx[y].

                A value of -1 means a vertex isn't matched to anything.  Moreover,
                x corresponds to rows of the cost matrix and y corresponds to the
                columns of the cost matrix.  So we are matching X to Y.
        */


        // Create an initial feasible labeling.  Moreover, in the following
        // code we will always have: 
        //     for all valid x and y:  lx[x] + ly[y] >= cost.get(x,y)
        lx.growTo(cost.nc);
        ly.assign(cost.nc,0);
        for (int x = 0; x < cost.nr; ++x){
            // JO: old code: lx[x] = max(rowm(cost,x)); // rowm(m, i) returns ith row of m as a matrix
            int max=-1;
            int val;
            for(int i=0; i<cost.nc; ++i){
                val = cost.get(x,i);
                if(max<val){max=val;}
            }
            lx[x]=max;
        }
            

        // Now grow the match set by picking edges from the equality subgraph until
        // we have a complete matching.
        for (int match_size = 0; match_size < cost.nc; ++match_size){
            std::deque<int> q; // JO: todo: convert to vec

            // Empty out the S and T sets
            S.assign(cost.nc, false);
            T.assign(cost.nc, false);

            // clear out old slack values
            slack.assign(cost.nc, std::numeric_limits<int>::max());
            slackx.growTo(cost.nc);
            /*
                slack and slackx are maintained such that we always
                have the following (once they get initialized by compute_slack() below):
                    - for all y:
                        - let x == slackx[y]
                        - slack[y] == lx[x] + ly[y] - cost.get(x,y)
            */

            aug_path.assign(cost.nc, -1);

            for (int x = 0; x < cost.nc; ++x){
                // If x is not matched to anything
                if (xy[x] == -1){
                    q.push_back(x);
                    S[x] = true;

                    compute_slack(x, slack, slackx, cost, lx, ly);
                    break;
                }
            }

            int x_start = 0;
            int y_start = 0;

            // Find an augmenting path.  
            bool found_augmenting_path = false;
            while (!found_augmenting_path){
                while (q.size() > 0 && !found_augmenting_path){
                    int x = q.front();
                    q.pop_front();
                    for (int y = 0; y < cost.nc; ++y){
                        if (cost.get(x,y) == lx[x] + ly[y] && !T[y]){
                            // if vertex y isn't matched with anything
                            if (yx[y] == -1){
                                y_start = y;
                                x_start = x;
                                found_augmenting_path = true;
                                break;
                            }

                            T[y] = true;
                            q.push_back(yx[y]);

                            aug_path[yx[y]] = x;
                            S[yx[y]] = true;
                            compute_slack(yx[y], slack, slackx, cost, lx, ly);
                        }
                    }
                }

                if (found_augmenting_path)
                    break;

                // Since we didn't find an augmenting path we need to improve the 
                // feasible labeling stored in lx and ly.  We also need to keep the
                // slack updated accordingly.
                int delta = std::numeric_limits<int>::max();
                for ( int i = 0; i < T.size(); ++i){
                    if (!T[i])
                        delta = std::min(delta, slack[i]);
                }
                for ( int i = 0; i < T.size(); ++i){
                    if (S[i])
                        lx[i] -= delta;

                    if (T[i])
                        ly[i] += delta;
                    else
                        slack[i] -= delta;
                }

                q.clear();
                for (int y = 0; y < cost.nc; ++y){
                    if (!T[y] && slack[y] == 0){
                        // if vertex y isn't matched with anything
                        if (yx[y] == -1){
                            x_start = slackx[y];
                            y_start = y;
                            found_augmenting_path = true;
                            break;
                        }else{
                            T[y] = true;
                            if (!S[yx[y]]){
                                q.push_back(yx[y]);

                                aug_path[yx[y]] = slackx[y];
                                S[yx[y]] = true;
                                compute_slack(yx[y], slack, slackx, cost, lx, ly);
                            }
                        }
                    }
                }
            } // end while (!found_augmenting_path)

            // Flip the edges aint the augmenting path.  This means we will add one more
            // item to our matching.
            for (int cx = x_start, cy = y_start, ty; cx != -1; cx = aug_path[cx], cy = ty){
                ty = xy[cx];
                yx[cy] = cx;
                xy[cx] = cy;
            }
        }

        int result = 0;
        for (int i = 0; i < xy.size(); ++i){
            result += cost.get(i, xy[i]);
        }
        return result; // todo: check whether the resulting value corresponds to the number of true/false/unknown in the symmetric clause
    }
}

