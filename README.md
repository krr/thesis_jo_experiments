# Experiment repository for thesis "Exploiting Symmetry in Model Expansion for Predicate and Propositional Logic #

The experimental results are organized by chapter, though Chapter 5 encompasses some experiments also presented in Chapter 3.

The folders contain the following:
> - Binary files used to run the experiments
> - Source files from which binaries were compiled
> - Instances used in the experiments
> - Summary files of experimental results
> - Optionally: detailed raw experimental results
> - Optionally: shell scripts
