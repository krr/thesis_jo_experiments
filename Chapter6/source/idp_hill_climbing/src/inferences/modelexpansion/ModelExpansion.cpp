/*****************************************************************************
 * Copyright 2010-2012 Katholieke Universiteit Leuven
 *
 * Use of this software is governed by the GNU LGPLv3.0 license
 *
 * Written by Broes De Cat, Bart Bogaerts, Stef De Pooter, Johan Wittocx,
 * Jo Devriendt, Joachim Jansen and Pieter Van Hertum 
 * K.U.Leuven, Departement Computerwetenschappen,
 * Celestijnenlaan 200A, B-3001 Leuven, Belgium
 ****************************************************************************/

#include "ModelExpansion.hpp"
#include "inferences/SolverConnection.hpp"

#include "structure/StructureComponents.hpp"

#include "theory/Query.hpp"
#include "inferences/querying/Query.hpp"
#include "inferences/grounding/Grounding.hpp"
#include "inferences/SolverInclude.hpp"
#include "groundtheories/GroundTheory.hpp"
#include "inferences/grounding/GroundTranslator.hpp"
#include "inferences/modelexpansion/TraceMonitor.hpp"
#include "inferences/functiondetection/FunctionDetection.hpp"
#include "errorhandling/error.hpp"
#include "creation/cppinterface.hpp"
#include "utils/ResourceMonitor.hpp"
#include "DefinitionPostProcessing.hpp"
//#include "theory/TheoryUtils.hpp"

using namespace std;

MXResult ModelExpansion::doModelExpansion(AbstractTheory* theory, Structure* structure, Vocabulary* outputvoc,
		TraceMonitor* tracemonitor, const MXAssumptions& assumeFalse) {
	auto m = createMX(theory, structure, nullptr, outputvoc, nullptr, tracemonitor, assumeFalse);
	return m->expand();
}

MXResult ModelExpansion::doMinimization(AbstractTheory* theory, Structure* structure, Term* term, Vocabulary* outputvoc,
		TraceMonitor* tracemonitor, const MXAssumptions& assumeFalse) {
	if (term == NULL) {
		throw IdpException("Unexpected NULL-pointer.");
	}
	if (not term->freeVars().empty()) {
		throw IdpException("Cannot minimized over term with free variables.");
	}
	auto m = createMX(theory, structure, term, outputvoc, nullptr, tracemonitor, assumeFalse);
	return m->expand();
}

const DomainElement* evaluateObjective(Structure* solution, Term* objective, std::vector<const Definition*>& objDefs) {
	Theory* deftheo = new Theory("wrapper_theory", solution->vocabulary(), ParseInfo());
	for (auto def : objDefs) {
		FormulaUtils::removeInterpretationOfDefinedSymbols(def, solution);
		deftheo->add(def->clone());
	}
	solution->clean();
	auto calcdefresult = CalculateDefinitions::doCalculateDefinitions(deftheo, solution); // TODO: calcdefresult should return a boolean or a list of unprocessed definitions
	Assert(calcdefresult._hasModel);
	const DomainElement* res = evaluate(objective, solution);
	Assert(res->type() == DomainElementType::DET_INT);
	return res;
}

const DomainElement* doHillClimbing(Structure* sol, Term* obj, std::vector<const Definition*>& objDefs, 
		const std::vector<const SwapSymmetry*>& swaps, const std::vector<const Theory*>& relaxedConstraints){
	
	Assert(swaps.size()==relaxedConstraints.size());
	long objval = evaluate(obj, sol)->value()._int;

	// initiate hill climbing data structures
	std::vector<unsigned int> indexes;
	for(unsigned int i=0; i<swaps.size(); ++i){
		indexes.push_back(i);
	}

	// perform hill climbing
	while (indexes.size() > 0) {
		unsigned long randIdx = rand() % indexes.size();
		auto currentMove = swaps[indexes[randIdx]];
		auto currentConstraints = relaxedConstraints[indexes[randIdx]];

		if (getOption(IntType::VERBOSE_LOCALSEARCH) > 1) {
			currentMove->print(std::clog);
		}

		currentMove->apply(sol);
		bool satisfiesconstraints = true;
		long newval=0;
		if(currentConstraints==nullptr || satisfies(currentConstraints,sol)){
			newval = evaluateObjective(sol, obj, objDefs)->value()._int;
		}else{
			if (getOption(IntType::VERBOSE_LOCALSEARCH) > 2) {
				std::clog << "Move violates relaxed constraints." << std::endl;
			}
			satisfiesconstraints=false;
		}
		
		if(satisfiesconstraints && newval<objval){
			if (getOption(IntType::VERBOSE_LOCALSEARCH) > 1) {
				std::clog << "Succesful move! New objective value: " << newval << std::endl;
			}
			// accept solution
			objval = newval;
			// reinitialize moves
			indexes.clear();
			for(unsigned int i=0; i<swaps.size(); ++i){
				indexes.push_back(i);
			}
		}else{
			if (getOption(IntType::VERBOSE_LOCALSEARCH) > 2) {
				std::clog << "Unsuccesful move. Rolling back..." << std::endl;
			}
			// roll back move
			currentMove->applyInverse(sol);
			// remove move from list
			indexes[randIdx] = indexes.back();
			indexes.pop_back();
		}
	}

	return evaluateObjective(sol, obj, objDefs); // NOTE: this also fixes the defined objective symbols to the right value again
}

MXResult ModelExpansion::doMinimizationWithLocalSearch(AbstractTheory* theory, Structure* structure, Term* term, Vocabulary* outputvoc,
		TraceMonitor* tracemonitor, const MXAssumptions& assumeFalse) {
	if (term == NULL) {
		throw IdpException("Unexpected NULL-pointer.");
	}
	if (not term->freeVars().empty()) {
		throw IdpException("Cannot minimized over term with free variables.");
	}

	Theory* theo = dynamic_cast<Theory*> (theory->clone()); // clone needed to avoid segmentation fault in calling method :/ TODO: fix

	// removing some total definitions from theory, to interpret as many symbols as possible
	auto calcdefresult = CalculateDefinitions::doCalculateDefinitions(theo, structure); // TODO: calcdefresult should return a boolean or a list of unprocessed definitions

	// TODO: detect global symmetries!

	// find first solution
	MXResult result = createMX(theo, structure, nullptr, outputvoc, nullptr, tracemonitor, assumeFalse)->expand();
	if (result.unsat) {
		return result;
	}
	// first local search over all constraints
	if (getOption(IntType::VERBOSE_LOCALSEARCH) > 0) {
		std::clog << "First objective value: " << evaluate(term, result._models[0])->value()._int << std::endl;
	}

	// detecting neighborhoods
	std::vector<InterchangeabilityGroup*> hardNeighborhood;
	std::vector<InterchangeabilityGroup*> relaxedNeighborhood;
	std::vector<const Definition*> termDefs;
	Theory* relaxedTheory = new Theory("__RELAXED CONSTRAINTS__",theo->vocabulary(),theo->pi());
	
	if (getGlobal()->getOptions()->localSearch() != LocalSearch::NONE) {
		detectNeighborhoods(theo, structure, term, termDefs, hardNeighborhood, relaxedNeighborhood, relaxedTheory); // note: relaxedNeighborhood is empty when the relaxation option is off
	}
	std::vector<const SwapSymmetry*> swaps;
	std::vector<const Theory*> relaxedConstraints;
	for (auto icg : hardNeighborhood) {
		icg->addSwapSymmetries(swaps);
	}
	unsigned int i=0;
	for(; i<swaps.size(); ++i){
		relaxedConstraints.push_back(nullptr);
	}
	for (auto icg : relaxedNeighborhood) {
		icg->addSwapSymmetries(swaps);
	}
	for(; i<swaps.size(); ++i){
		relaxedConstraints.push_back(relaxedTheory);
	}
	srand(getOption(IntType::RANDOMSEED));

	while (true) {
		// solution found, do local search
		auto sol = result._models[0];
		if (getOption(IntType::VERBOSE_LOCALSEARCH) > 1) {
			std::clog << "Initial solution: \n";
			sol->put(std::clog);
		}

		// first local search over all constraints
		if (getOption(IntType::VERBOSE_LOCALSEARCH) > 0) {
			std::clog << "Starting local search...\nInitial objective value: " << evaluate(term, sol)->value()._int << std::endl;
		}
		const DomainElement* boundEl = doHillClimbing(sol, term, termDefs, swaps, relaxedConstraints);
		// add resulting bound
		DomainTerm* boundTerm = new DomainTerm(term->sort(), boundEl, {
		});
		EqChainForm* boundForm = new EqChainForm(term, CompType::LT, boundTerm);
		theo->add(boundForm);
		if (getOption(IntType::VERBOSE_LOCALSEARCH) > 0) {
			std::clog << "Strengthened objective value: " << boundEl->value()._int << std::endl;
			if (getOption(IntType::VERBOSE_LOCALSEARCH) > 1) {
				sol->put(std::clog);
			}
		}

		// use solution for warm restart
		result = createMX(theo, structure, nullptr, outputvoc, sol, tracemonitor, assumeFalse)->expand();

		if (result.unsat) {
			// optimality proven
			result.unsat = false;
			result._models.push_back(sol);
			result._optimumfound = true;
			result._optimalvalue = boundEl->value()._int;
			
			delete relaxedTheory;
			return result;
		} else {
			// continue search
			delete sol;
		}
	}

	delete relaxedTheory;
	return result;
}

shared_ptr<ModelExpansion> ModelExpansion::createMX(AbstractTheory* theory, Structure* structure, Term* term, Vocabulary* outputvoc, Structure* startstruc,
		TraceMonitor* tracemonitor, const MXAssumptions& assumeFalse) {
	if (theory == NULL || structure == NULL) {
		throw IdpException("Unexpected NULL-pointer.");
	}
	auto t = dynamic_cast<Theory*> (theory); // TODO handle other cases
	if (t == NULL) {
		throw notyetimplemented("Modelexpansion of already ground theories");
	}
	if (structure->vocabulary() != theory->vocabulary()) {
		if (VocabularyUtils::isSubVocabulary(structure->vocabulary(), theory->vocabulary())) {
			structure->changeVocabulary(theory->vocabulary());
		} else {
			throw IdpException("Modelexpansion requires that the structure interprets (a subvocabulary of) the vocabulary of the theory.");
		}
	}
	if (term != NULL && not VocabularyUtils::isSubVocabulary(term->vocabulary(), theory->vocabulary())) {
		throw IdpException("Modelexpansion requires that the minimization term ranges over (a subvocabulary of) the vocabulary of the theory.");
	}
	auto m = shared_ptr<ModelExpansion>(new ModelExpansion(t, structure, term, tracemonitor, assumeFalse));
	m->setOutputVocabulary(outputvoc);
	m->setStartStructure(startstruc);
	if (getGlobal()->getOptions()->symmetryBreaking() != SymmetryBreaking::NONE && getOption(NBMODELS) != 1) {
		Warning::warning("Cannot generate models symmetrical to models already found! More models might exist.");
	}
	return m;
}

ModelExpansion::ModelExpansion(Theory* theory, Structure* structure, Term* minimize, TraceMonitor* tracemonitor, const MXAssumptions& assumeFalse)
: _theory(theory),
_structure(structure),
_tracemonitor(tracemonitor),
_minimizeterm(minimize),
_outputvoc(nullptr),
_startstruc(nullptr),
_assumeFalse(assumeFalse) {
}

void ModelExpansion::setOutputVocabulary(Vocabulary* v) {
	if (not VocabularyUtils::isSubVocabulary(v, _theory->vocabulary())) {
		throw IdpException("The output-vocabulary of model expansion can only be a subvocabulary of the theory.");
	}
	_outputvoc = v;
}

void ModelExpansion::setStartStructure(Structure* s) {
	if (s != nullptr && not VocabularyUtils::isSubVocabulary(s->vocabulary(), _theory->vocabulary())) {
		throw IdpException("Modelexpansion requires that the startstructure ranges over (a subvocabulary of) the vocabulary of the theory.");
	}
	_startstruc = s;
}

Structure* handleSolution(Structure const * const structure, const MinisatID::Model& model, AbstractGroundTheory* grounding, StructureExtender* extender,
		Vocabulary* outputvoc, const std::vector<Definition*>& defs);

class SolverTermination : public TerminateMonitor {
private:
	MinisatID::ModelExpand* solver;
public:

	SolverTermination(MinisatID::ModelExpand* solver)
	: solver(solver) {
	}

	void notifyTerminateRequested() {
		solver->notifyTerminateRequested();
	}
};

#define cleanup \
		grounding->recursiveDelete();\
		clonetheory->recursiveDelete();\
		getGlobal()->removeTerminationMonitor(terminator);\
		delete (extender);\
		delete (terminator);\
		delete (newstructure);\
		delete (voc); \
		delete (data);\
		delete (mx);

litlist MXAssumptions::toLitList(GroundTranslator* trans) const{
	litlist assumptions;
	for(auto p: assumeAllFalse){
		for(auto atom: trans->getIntroducedLiteralsFor(p)){ // TODO should be introduced ATOMS
			assumptions.push_back(-abs(atom.second));
		}
		std::vector<Variable*> vars;
		std::vector<Term*> varterms;
		for(uint i=0; i<p->arity(); ++i){
			vars.push_back(new Variable(p->sorts()[i]));
			varterms.push_back(new VarTerm(vars.back(), {}));
		}
		auto table = Querying::doSolveQuery(new Query("", vars, new PredForm(SIGN::POS, p, varterms, {}), {}), trans->getConcreteStructure(), trans->getSymbolicStructure());
		for(auto i=table->begin(); not i.isAtEnd(); ++i){
			auto atom = trans->translateNonReduced(p, *i);
			assumptions.push_back(-abs(atom));
		}
	}
	for(auto pf : assumeFalse){
		assumptions.push_back(-trans->translateNonReduced(pf.symbol, pf.args));
	}
	for(auto pt : assumeTrue){
		assumptions.push_back(trans->translateNonReduced(pt.symbol, pt.args));
	}
	return assumptions;
}

MXResult ModelExpansion::expand() const {
	auto mxverbosity = max(getOption(IntType::VERBOSE_SOLVING), getOption(IntType::VERBOSE_SOLVING_STATISTICS));
	auto data = SolverConnection::createsolver(getOption(IntType::NBMODELS));
	auto targetvoc = _outputvoc == NULL ? _theory->vocabulary() : _outputvoc;
	auto clonetheory = _theory->clone();
	auto newstructure = _structure->clone();
	auto voc = new Vocabulary(createName());
	voc->add(clonetheory->vocabulary());
	voc->add(newstructure->vocabulary());
	newstructure->changeVocabulary(voc);
	clonetheory->vocabulary(voc);

	if (getOption(BoolType::FUNCDETECT)) {
		if (getOption(IntType::VERBOSE_GROUNDING) > 0) {
			logActionAndTime("Starting function detection at ");
		}

		FunctionDetection::doDetectAndRewriteIntoFunctions(clonetheory);
		newstructure->changeVocabulary(clonetheory->vocabulary());
		//Note: outputvoc remains unchanged

		if (getOption(IntType::VERBOSE_GROUNDING) > 0) {
			logActionAndTime("Finished function detection at ");
		}
	}

	DefinitionUtils::splitDefinitions(clonetheory);

	std::vector<Definition*> postprocessdefs;
	if (getOption(POSTPROCESS_DEFS)) {
		postprocessdefs = simplifyTheoryForPostProcessableDefinitions(clonetheory, _minimizeterm, _structure, voc, targetvoc);
	}
	if (getOption(SATISFIABILITYDELAY)) { // Add non-forgotten defs again, as top-down grounding might give a better result
		for (auto def : postprocessdefs) {
			clonetheory->add(def);
		}
		postprocessdefs.clear();
	}

	std::pair<AbstractGroundTheory*, StructureExtender*> groundingAndExtender = {NULL, NULL};
	try {
		groundingAndExtender = GroundingInference<PCSolver>::createGroundingAndExtender(clonetheory, newstructure, _outputvoc, _minimizeterm, _tracemonitor, getOption(IntType::NBMODELS) != 1, data);
	} catch (...) {
		if (getOption(VERBOSE_GROUNDING_STATISTICS) > 0) {
			logActionAndValue("effective-size", groundingAndExtender.first->getSize()); //Grounder::groundedAtoms());
		}
		throw;
	}
	auto grounding = groundingAndExtender.first;
	auto extender = groundingAndExtender.second;

	litlist assumptions = _assumeFalse.toLitList(grounding->translator());

	// use given start structure as starting lits for the solver
	litlist startlits;
	if (_startstruc != nullptr) {
		_startstruc->materialize();
		for (auto predinter : _startstruc->getPredInters()) {
			PredInter* predint = predinter.second;
			auto itertor = predint->ct()->begin();
			while (!itertor.isAtEnd()) {
				Lit l = grounding->translator()->translateNonReduced(predinter.first, *itertor);
				startlits.push_back(l);
				++itertor;
			}
			itertor = predint->cf()->begin();
			while (!itertor.isAtEnd()) {
				Lit l = grounding->translator()->translateNonReduced(predinter.first, *itertor);
				startlits.push_back(-l);
				++itertor;
			}
		}
		for (auto predinter : _startstruc->getFuncInters()) {
			PredInter* predint = predinter.second->graphInter();
			auto itertor = predint->ct()->begin();
			while (!itertor.isAtEnd()) {
				Lit l = grounding->translator()->translateNonReduced(predinter.first, *itertor);
				startlits.push_back(l);
				++itertor;
			}
			itertor = predint->cf()->begin();
			while (!itertor.isAtEnd()) {
				Lit l = grounding->translator()->translateNonReduced(predinter.first, *itertor);
				startlits.push_back(-l);
				++itertor;
			}
		}
	}

	// Run solver
	auto mx = SolverConnection::initsolution(data, getOption(NBMODELS), assumptions, startlits);
	auto startTime = clock();
	if (mxverbosity > 0) {
		logActionAndTime("Starting solving at ");
	}
	bool unsat = false;
	auto terminator = new SolverTermination(mx);
	getGlobal()->addTerminationMonitor(terminator);

	auto t = basicResourceMonitor([]() {
		return getOption(MXTIMEOUT);
	}, []() {
		return getOption(MXMEMORYOUT);
	}, [terminator]() {
		terminator->notifyTerminateRequested();
	});
	tthread::thread time(&resourceMonitorLoop, &t);

	MXResult result;
	try {
		mx->execute(); // FIXME wrap other solver calls also in try-catch
		unsat = mx->getSolutions().size() == 0;
		if (getGlobal()->terminateRequested()) {
			result._interrupted = true;
			getGlobal()->reset();
		}
	} catch (MinisatID::idpexception& error) {
		std::stringstream ss;
		ss << "Solver was aborted with message \"" << error.what() << "\"";
		t.requestStop();
		time.join();
		cleanup;
		throw IdpException(ss.str());
	} catch (UnsatException& ex) {
		unsat = true;
	} catch (...) {
		t.requestStop();
		time.join();
		throw;
	}

	t.requestStop();
	time.join();

	if (getOption(VERBOSE_GROUNDING_STATISTICS) > 0) {
		logActionAndValue("effective-size", groundingAndExtender.first->getSize());
		if (mx->getNbModelsFound() > 0) {
			logActionAndValue("state", "satisfiable");
		}
		std::clog.flush();
	}

	if (getGlobal()->terminateRequested()) {
		throw IdpException("Solver was terminated");
	}

	result._optimumfound = not result._interrupted;
	result.unsat = unsat;
	if (t.outOfResources()) {
		Warning::warning("Model expansion interrupted: will continue with the (single best) model(s) found to date (if any).");
		result._optimumfound = false;
		result._interrupted = true;
		getGlobal()->reset();
	}

	if (not t.outOfResources() && unsat) {
		MXResult result;
		result.unsat = true;
		for (auto lit : mx->getUnsatExplanation()) {
			auto symbol = grounding->translator()->getSymbol(lit.getAtom());
			auto args = grounding->translator()->getArgs(lit.getAtom());
			result.unsat_in_function_of_ct_lits.push_back({symbol, args});
		}
		cleanup;
		if (getOption(VERBOSE_GROUNDING_STATISTICS) > 0) {
			logActionAndValue("state", "unsat");
		}
		return result;
	}

	// Collect solutions
	std::vector<Structure*> solutions;
	if (_minimizeterm != NULL) { // Optimizing
		if (not unsat) {
			Assert(mx->getBestSolutionsFound().size() > 0);
			auto list = mx->getBestSolutionsFound();
			for (auto i = list.cbegin(); i < list.cend(); ++i) {
				solutions.push_back(handleSolution(newstructure, **i, grounding, extender, targetvoc, postprocessdefs));
			}
			auto bestvalue = evaluate(_minimizeterm->clone(), solutions.front());
			Assert(bestvalue != NULL && bestvalue->type() == DomainElementType::DET_INT);
			result._optimalvalue = bestvalue->value()._int;
			if (mxverbosity > 0) {
				logActionAndValue("bestvalue", result._optimalvalue);
				logActionAndValue("nrmodels", list.size());
				logActionAndTimeSince("total-solving-time", startTime);
			}
			if (getOption(VERBOSE_GROUNDING_STATISTICS) > 0) {
				logActionAndValue("bestvalue", result._optimalvalue);
				if (result._optimumfound) {
					logActionAndValue("state", "optimal");
				} else {
					logActionAndValue("state", "satisfiable");
				}
				std::clog.flush();
			}
		}
	} else if (not unsat) {
		if (getOption(VERBOSE_GROUNDING_STATISTICS) > 0) {
			logActionAndValue("state", "satisfiable");
		}
		auto abstractsolutions = mx->getSolutions();
		if (mxverbosity > 0) {
			logActionAndValue("nrmodels", abstractsolutions.size());
			logActionAndTimeSince("total-solving-time", startTime);
		}
		for (auto model = abstractsolutions.cbegin(); model != abstractsolutions.cend(); ++model) {
			solutions.push_back(handleSolution(newstructure, **model, grounding, extender, targetvoc, postprocessdefs));
		}
	}

	// Clean up: remove all objects that are only used here.
	cleanup;
	result._models = solutions;
	return result;
}

Structure* handleSolution(Structure const * const structure, const MinisatID::Model& model, AbstractGroundTheory* grounding, StructureExtender* extender,
		Vocabulary* outputvoc, const std::vector<Definition*>& postprocessdefs) {
	auto newsolution = structure->clone();
	SolverConnection::addLiterals(model, grounding->translator(), newsolution);
	SolverConnection::addTerms(model, grounding->translator(), newsolution);
	auto defs = postprocessdefs;
	if (extender != NULL && useLazyGrounding()) {
		auto moredefs = extender->extendStructure(newsolution);
		insertAtEnd(defs, moredefs);
		newsolution->clean();
	}
	computeRemainingDefinitions(defs, newsolution, outputvoc);
	newsolution->changeVocabulary(outputvoc);
	newsolution->clean();
	Assert(newsolution->isConsistent());
	return newsolution;
}
