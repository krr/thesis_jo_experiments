#include "Theory.hpp"
#include <fstream>
#include <sstream>
#include <iterator>
#include <iostream>
#include <algorithm>
#include <bitset>
#include "Graph.hpp"
#include "Algebraic.hpp"
#include "Breaking.hpp"

//=========================CNF==================================================

using namespace std;

void CNF::readCNF(std::string& filename) {
  if (verbosity > 0) {
    std::clog << "*** Reading CNF: " << filename << std::endl;
  }

  ifstream file(filename);
  if (!file) {
    gracefulError("No CNF file found.");
  }
  string line;
  set<uint> inclause = set<uint>();
  while (getline(file, line)) {
    if (line.front() == 'c') {
      // do nothing, this is a comment line
    } else if (line.front() == 'p') {
      string info = line.substr(6);
      istringstream iss(info);
      uint nbVars;
      iss >> nbVars;
      uint nbClauses;
      iss >> nbClauses;
      if (verbosity > 1) {
        std::clog << "CNF header stated " << nbVars << " vars and " << nbClauses << " clauses" << std::endl;
      }
			nVars = nbVars;
      clauses.reserve(nbClauses);
    } else {
      //  parse the clauses, removing tautologies and double lits
      istringstream iss(line);
      int l;
      while (iss >> l) {
        if (l == 0) {
          if (inclause.size() == 0) {
            gracefulError("Theory can not contain empty clause.");
          }
          bool isTautology = false;
          for (auto lit : inclause) {
            if (inclause.count(neg(lit)) > 0) {
              isTautology = true;
              break;
            }
          }
          if (not isTautology) {
            sptr<Clause> cl(new Clause(inclause));
            clauses.insert(cl);
          }
          inclause.clear();
        } else {
          if ((uint) abs(l) > nVars) {
            nVars = abs(l);
          }
          inclause.insert(encode(l));
        }
      }
    }
  }
}

CNF::CNF(std::string& filename) {
  readCNF(filename);
  if (verbosity > 0) {
    std::clog << "*** Creating first graph..." << std::endl;
  }
  graph = make_shared<Graph>(clauses);
  group = make_shared<Group>();
  if (verbosity > 0) {
    std::clog << "*** Detecting symmetry group..." << std::endl;
  }
  std::vector<sptr<Permutation> > symgens;
  graph->getSymmetryGenerators(symgens);
  for (auto symgen : symgens) {
    group->add(symgen);
  }
}

CNF::CNF(std::vector<sptr<Clause> >& clss, sptr<Group> grp) {
  clauses.insert(clss.cbegin(), clss.cend());
  graph = make_shared<Graph>(clauses);
  group = grp;
  for (uint l = 0; l < 2 * nVars; ++l) {
    if (not grp->permutes(l)) {
      graph->setUniqueColor(l);
    }
  }
}

CNF::~CNF() {
}

void CNF::print() {
  std::clog << "p cnf " << nVars << " " << clauses.size() << "\n";
  for (auto clause : clauses) {
    clause->print(std::clog);
  }
}

uint CNF::getSize() {
  return clauses.size();
}

void CNF::setSubCNF(sptr<Group> subgroup) {
  std::vector<sptr<Clause> > subclauses;
  for (auto cl : clauses) {
    for (auto lit : cl->lits) {
      if (subgroup->permutes(lit)) {
        subclauses.push_back(cl);
        break;
      }
    }
  }
  subgroup->theory = new CNF(subclauses, subgroup);
}

void CNF::cleanUp(){
  graph.reset();
  group.reset();
}

sptr<Graph> CNF::getGraph() {
  return graph;
}

sptr<Group> CNF::getGroup() {
  return group;
}

bool CNF::isSymmetry(Permutation& prm) {
  for (auto cl : clauses) {
    sptr<Clause> symmetrical(new Clause());
    if (!prm.getImage(cl->lits, symmetrical->lits)) {
      continue;
    }
    std::sort(symmetrical->lits.begin(), symmetrical->lits.end());
    if (clauses.count(symmetrical) == 0) {
      return false;
    }
  }
  return true;
}
