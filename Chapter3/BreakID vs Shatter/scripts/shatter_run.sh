#!/bin/bash

cnf=$1

(./shatter -s $cnf 3>&1 1>&2- 2>&3- ) 2> $cnf.brk | awk '/generators/ {print $3}' | tr '\012' ' '
