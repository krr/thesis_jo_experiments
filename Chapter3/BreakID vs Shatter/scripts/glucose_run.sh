#!/bin/bash

cnf=$1

/export/home1/NoCsBack/dtai/jodv/glucose_static $cnf 2>&1 | awk '/c restarts/ {print $4} /c decisions/ {print $4} /c conflicts/ {print $4} /c propagations/ {print $4} /SATISFIABLE/ {print $2}' | tr '\012' ','
