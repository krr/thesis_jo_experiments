#!/bin/bash

cnf=$1

(./BreakID $cnf -no-row -no-bin -s 999999999 -no-relaxed 3>&1 1>&2- 2>&3- ) 2> $cnf.brk | awk '
/**** symmetry generators detected:/ {print $5}
/**** subgroups detected:/ {print $4} 
/**** matrices detected:/ {print $4} 
/**** row swaps detected:/ {print $5} 
/**** extra binary symmetry breaking clauses added:/ {print $8}
/**** regular symmetry breaking clauses added:/ {print $7}
/**** row interchangeability breaking clauses added:/ {print $7}
/**** total symmetry breaking clauses added:/ {print $7}
/**** auxiliary variables introduced:/ {print $5}
' | tr '\012' ','
